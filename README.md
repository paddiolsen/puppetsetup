# Puppet setup

## Installering af Jira server via puppet - Forge er til rådighed
### hvilken forge skal man vælge?
Der er tre jira moduler som ligger i toppen af søgningen 
1. "jira" lavet af "puppet" version "5.1.0" udgivet 20 august 2021 downloads "208,888" kvalitets score på 5.0 - godkendelse er "approved"

2. "jira" lavet af "mkrakowitzer" version "999.999.999" udgivet 25 april 2015 downloads "54,663" kvalitets score "2.8" 

3. "jira" lavet af "elsonrodriguez" version "0.0.1" udgivet "22 december 2011" downloads "10,224" kvalitets score "3.8"

For at starte fra bunden af. 
nummer 3 - Der er ingen information fra udbydderen, hvilket gør det svært at arbejde med. versionen virker til at være en beta version som egenligt aldrig er gået i produktion samt den sidste version er meget gammel 2011. Den har den laveste antal downloads af de tre udvalgte men den næst højeste score

nummer 2 - Udbydderen informere at denne version er fjernet fra Forge og flytte til community Derfor er versionen også 999.999.999 Der er linket til en github repo som ejes af den samme bruger som nummer1 og indeholder samme informationer i README.md som informationen i nummer1 på forge.

nummmer 1 - Har en dybde gående information om det pågældende modul, hvilket gør det nemmer at arbejde med da den nyeste version er fra 2021 den er klar den med flest downloads og højest kvalitets score

### konklusion
Valget går på nummer 1.
Nummer 1 er hyppigt opdateret, der er mange som bruger modulet og den har godkendelse approvedhvilket foræller at kvalteten er god. Selvom man filtrere efter approvede moduler, så er der ikke andre moduler til jira.

## Installer jira med puppet
### Brug forge men ikke r10k eller hiera

> Det er første gang jeg arbejder med forge moduler istedet for at lave modulet selv.
Hvad er et forge module?
- Det er færdiglavede moduler som installeres på master og inkluderet i din .pp file 
- Man finder forge moduler ved at søge på forge.puppet.com eller via commandline ``` puppet module search {modulename}```
- Forge moduler kan også installeres via bolt, r10k, hiera eller direkte download



## Download jira
Da det skal laves uden r10k eller hiera så downloades jira manuelt til master.
På forge siden til jira findes commandline til at installer jira
```> puppet module install puppet-jira --version 5.1.0```

Ved brug af puppetlearning vm så peger downloads destinationen på localhost. Hvordan vælger man anden destination?
https://puppet.com/docs/puppet/6/modules_installing.html

Ved at installer et fra et ande modul repo 
```puppet module install --module_repository https://forgeapi.puppet.com  puppet-jira```



## installer jira

Jeg laver en class til jira for at seperere dem og der næsteincludere jira class i site.pp.
node bliver sat til domain for puppet agent maskinen.

Error når jeg installer jira på agent. "JIRA versions older than 8.0.0 are no longer supported. Please use an older version of this module to upgrade first"
for at gøre det nemmer for mig selv har jeg indsat jira class i site.pp 

> For at imødekomme dette problem skal jeg installere en tidligere version.


``` 
node 'agent.puppet.vm' {
class { 'jira':
  version    => '8.13.5',
  javahome => '/opt/java',
  }
}
```

Jeg forsøger at bruge andre tidliger versioner end 8.0.0 hvilket giver det samme resultat. I setup angives der at jdk skal er et krav, så forsøger at indsætte jdk.

``` 
node 'agent.puppet.vm' {
class { 'jira':
  java_package  => 'java-11-openjdk-headless',
  javahome    => '/usr/lib/jvm/jre-11-opendjk/',
  version => '7.16.0',
  }
}
```

Det virker til at version nummeret er indlagt i modules/jira/manifest/init.pp
Jeg forsøger at ændre versionsnummeret.
> stadig samme fejl

init.pp angiver 
``` 
if $product != 'servicedesk' and versioncmp($jira::version, '8.0.0') < 0 
```

Men selvom jira bliver sat til 0.0.0 så angiver den stadig samme fejl også selvom man fjerne version nummeret helt.





## Håndtering af version
ved at styre manifesten via git kan du styre opdatering af versionen - dog gøres der opmærksom på at der skal fortages bakup ved større versions spring.
